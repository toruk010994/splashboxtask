<?php
/**
 * Created by PhpStorm.
 * User: Alexander
 * Date: 04.08.2019
 * Time: 22:29
 */

include_once __DIR__."/vendor/autoload.php";

use Splashbox\Family\Cat;
use Splashbox\Family\Dog;
use Splashbox\Family\Person;

try {
    $dog = new Dog("David");
    $cat = new Cat("Yt'");
    $alex = new Person("Alex", $dog);
    $vlad = new Person("Vlad", $cat);

    $alex->hello()->owns();
    $dog->noise();

    echo PHP_EOL;

    $vlad->hello()->owns();
    $cat->sit()->noise();
} catch (ReflectionException $e) {
}