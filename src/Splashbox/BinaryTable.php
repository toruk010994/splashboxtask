<?php
/**
 * Created by PhpStorm.
 * User: Alexander
 * Date: 01.08.2019
 * Time: 22:04
 */

/**
 * @param int $cNum     column number
 * @param int $rNum     row number
 */
function showBinatyTable(int $cNum, int $rNum) : void
{
    for ($r = 0; $r < $rNum; $r++) {
        $row = "┆ ";
        $delim = "+-";

        for ($c = 0; $c < $cNum; $c++) {
            $n = rand(0, 100) > 50 ? 1 : 0;

            $row .= "${n}";
            $delim .= "-";
            if ($c < $cNum - 1) {
                $row .= " ┆ ";
                $delim .= "-+-";
            }
        }

        $row .= " ┆";
        $delim .= "-+";

        if (!$r)
            echo $delim.PHP_EOL;
        echo $row.PHP_EOL;
        echo $delim.PHP_EOL;
    }
}