<?php
/**
 * Created by PhpStorm.
 * User: Alexander
 * Date: 04.08.2019
 * Time: 22:15
 */

namespace Splashbox\Family;

class Cat extends Pet
{
    protected  $word = "meow";

    public function __construct($name)
    {
        $this->name = $name;
        parent::__construct();
    }

    public function sit()
    {
        echo "{$this->name} the {$this->ref->getShortName()} sits on the chair.".PHP_EOL;
        return $this;
    }
}