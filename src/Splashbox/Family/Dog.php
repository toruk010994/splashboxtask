<?php
/**
 * Created by PhpStorm.
 * User: Alexander
 * Date: 04.08.2019
 * Time: 22:11
 */

namespace Splashbox\Family;

class Dog extends Pet
{
    private $words = [
        "arf",
        "woof",
        "hav",
        "vov",
        "gong",
        "wang"
    ];
    protected  $word;

    public function __construct($name)
    {
        $this->name = $name;
        $index = rand(0, count($this->words) - 1);
        $this->word = $this->words[$index];
        parent::__construct();
    }
}