<?php
/**
 * Created by PhpStorm.
 * User: Alexander
 * Date: 04.08.2019
 * Time: 21:45
 */

namespace Splashbox\Family;


use ReflectionClass;

class Person
{
    public $name;
    public $pet;
    public $ref;

    /**
     * Person constructor.
     * @param string $name
     * @param $pet
     * @throws \ReflectionException
     */
    public function __construct(string $name, Pet $pet)
    {
        $this->name = $name;
        $this->pet = $pet;
        $this->ref = new ReflectionClass($this);
    }

    public function hello()
    {
        echo "{$this->fullName()} tells you 'Hello!'".PHP_EOL;
        return $this;
    }

    public function owns()
    {
        echo "{$this->fullName()} owns {$this->pet->fullName()}".PHP_EOL;
        return $this;
    }

    public function fullName()
    {
        return "{$this->name} the {$this->ref->getShortName()}";
    }
}