<?php
/**
 * Created by PhpStorm.
 * User: Alexander
 * Date: 04.08.2019
 * Time: 21:51
 */

namespace Splashbox\Family;


use ReflectionClass;

abstract class Pet
{
    protected $word;
    protected $name;
    protected $ref;

    /**
     * Pet constructor.
     * @throws \ReflectionException
     */
    public function __construct()
    {
        $this->ref = new ReflectionClass($this);
    }

    function noise()
    {
        $max = rand(1, 10);
        $noise = "";
        for ($i = 0; $i < $max; $i++) {
            $noise .= $this->word;
            if ($i < $max - 1)
                $noise .= "-";
        }
        echo "{$this->name} the {$this->ref->getShortName()} makes some noise: {$noise}".PHP_EOL;
        return $this;
    }

    public function fullName()
    {
        return "{$this->name} the {$this->ref->getShortName()}";
    }
}